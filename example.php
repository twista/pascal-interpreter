<?php

require __DIR__.'/src/Lexer.php';
require __DIR__.'/src/Analyzer.php';
require __DIR__.'/src/GrammarParser.php';


$grammarParser = Twista\Grammar\Parser::fromFile('./src/gramatic_rules.def');



$str = file_get_contents('test.pas');

// echo('<pre>'); print_r($str); echo('</pre>');


$lexer = new Twista\Lexer();
$analyzer = new Twista\Analyzer($lexer,$grammarParser);

$tokens = $analyzer->parse($str);
$analyzer->analyze($tokens);

// echo('<pre>'); var_dump($tokens); echo('</pre>');
