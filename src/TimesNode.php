<?php

class TimesNode extends SyntaxTreeNode {
    protected $l, $r;

    function __construct($l, $r) {
        $this->l = $l;
        $this->r = $r;
    }

    function getLeft() {
        return $this->l;
    }

    function getRight() {
        return $this->r;
    }

    function accept(ISyntaxTreeVisitor $visitor, $context) {
        return $visitor->visitTimes($this, $context);
    }
}