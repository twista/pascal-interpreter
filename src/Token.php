<?php

class Token {
    protected $name;
    protected $content;

    function __construct($name, $content) {
        $this->name = $name;
        $this->content = $content;
    }

    function getName() {
        return $this->name;
    }

    function getContent() {
        return $this->content;
    }
}