<?php

interface ISyntaxTreeVisitor {
    function visitInteger(IntegerNode $node, $context);
    function visitVariable(VariableNode $node, $context);
    function visitPlus(PlusNode $node, $context);
    function visitMinus(MinusNode $node, $context);
    function visitTimes(TimesNode $node, $context);
    function visitDivide(DivideNode $node, $context);
    function visitModulo(ModuloNode $node, $context);
}