<?php

class IntegerNode extends SyntaxTreeNode {
    protected $value;

    function __construct($value) {
        $this->value = $value;
    }

    function getValue() {
        return $this->value;
    }

    function accept(ISyntaxTreeVisitor $visitor, $context) {
        return $visitor->visitInteger($this, $context);
    }
}