<?php

class Evaluator implements ISyntaxTreeVisitor {
    function visitInteger(IntegerNode $node, $context) {
        return $node->getValue();
    }

    function visitVariable(VariableNode $node, $context) {
        return $context[$node->getName()];
    }

    function visitPlus(PlusNode $node, $context) {
        return $node->getLeft()->accept($this, $context) + $node->getRight()->accept($this, $context);
    }

    function visitMinus(MinusNode $node, $context) {
        return $node->getLeft()->accept($this, $context) - $node->getRight()->accept($this, $context);
    }

    function visitTimes(TimesNode $node, $context) {
        return $node->getLeft()->accept($this, $context) * $node->getRight()->accept($this, $context);
    }

    function visitDivide(DivideNode $node, $context) {
        return (int)($node->getLeft()->accept($this, $context) / $node->getRight()->accept($this, $context));
    }

    function visitModulo(ModuloNode $node, $context) {
        return $node->getLeft()->accept($this, $context) % $node->getRight()->accept($this, $context);
    }
}